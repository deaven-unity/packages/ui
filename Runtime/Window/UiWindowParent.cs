using System.Collections.Generic;
using Deaven.Ui.Window.Behaviours;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Deaven.Ui.Window
{
    public class UiWindowParent : SerializedMonoBehaviour
    {
        [ShowInInspector, ReadOnly]
        public static List<UiWindowParent> OpenWindows = new List<UiWindowParent>();

        [ShowInInspector]
        public static int StartSortingIndex = 1;

        [SerializeField] private IOpenBehaviour _openBehaviour;
        [SerializeField] private ICloseBehaviour _closeBehaviour;
        [SerializeField] private UiWindow window;

        [SerializeField, ReadOnly]
        private bool _isOpen;
        
        [FoldoutGroup("Events")]
        public UnityEvent OnOpen;
        [FoldoutGroup("Events")]
        public UnityEvent OnClose;

        public bool IsOpen => _isOpen;

        private void Start()
        {
            window.Canvas.overrideSorting = true;
            window.Canvas.sortingOrder = StartSortingIndex;
            
            if (window.gameObject.activeSelf)
                _isOpen = true;
            else
                _isOpen = false;
        }

        [Button, DisableInEditorMode]
        public void Open()
        {
            if (IsOpen)
                return;
            
            _isOpen = true;
            OnOpen?.Invoke();

            window.Canvas.sortingOrder = StartSortingIndex + OpenWindows.Count;
            
            _openBehaviour.Open(window).OnComplete(() =>
            {
                OpenWindows.Add(this);
                window.CanvasGroup.interactable = true;
                window.gameObject.SetActive(true);
            });
        }

        [Button, DisableInEditorMode]
        public void Close()
        {
            if (!IsOpen)
                return;
            
            _isOpen = false;
            OnClose?.Invoke();
            
            window.CanvasGroup.interactable = false;
            
            _closeBehaviour.Close(window).OnComplete(() =>
            {
                OpenWindows.Remove(this);
                window.gameObject.SetActive(false);
            });
        }
        
        [Button, DisableInEditorMode]
        public void Toggle()
        {
            if (_isOpen)
                Close();
            else
                Open();
        }
    }
}
