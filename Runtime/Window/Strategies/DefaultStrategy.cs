using Deaven.Ui.Window.Behaviours;
using DG.Tweening;

namespace Deaven.Ui.Window.Strategies
{
    public class DefaultStrategy : IOpenBehaviour, ICloseBehaviour
    {
        public Tween Open(UiOpenCloseable target)
        {
            return DOTween.To(() => target.CanvasGroup.alpha, x => target.CanvasGroup.alpha = x, 1f, 0);
        }

        public Tween Close(UiOpenCloseable target)
        {
            return DOTween.To(() => target.CanvasGroup.alpha, x => target.CanvasGroup.alpha = x, 0f, 0);
        }
    }
}