using Deaven.Ui.Window.Behaviours;
using DG.Tweening;
using UnityEngine;

namespace Deaven.Ui.Window.Strategies
{
    public class ScaleStrategy : IOpenBehaviour, ICloseBehaviour
    {
        [SerializeField] private Vector3 _from;
        [SerializeField] private Vector3 _to;
        [SerializeField] private float delay;
        [SerializeField] private float duration;
        [SerializeField] private Ease easeType;

        public Tween Open(UiOpenCloseable target)
        {
            target.gameObject.SetActive(true);
            
            Sequence sequence = DOTween.Sequence();

            sequence.Append(Fade(target, 1f));
            sequence.Join(Scale(target, _from, _to));

            return sequence;
        }

        public Tween Close(UiOpenCloseable target)
        {
            Sequence sequence = DOTween.Sequence();

            sequence.Append(Scale(target, _from, _to));
            sequence.Join(Fade(target, 0));

            return sequence;
        }

        private Tween Scale(UiOpenCloseable target, Vector3 from, Vector3 to)
        {
            target.RectTransform.localScale = from;
            Tween scale = target.RectTransform.DOScale(to, duration).SetEase(easeType).SetDelay(delay);

            return scale;
        }

        private Tween Fade(UiOpenCloseable target, float endValue)
        {
            Tween fade = DOTween
                .To(() => target.CanvasGroup.alpha, x => target.CanvasGroup.alpha = x, endValue, duration)
                .SetEase(easeType).SetDelay(delay);

            return fade;
        }
    }
}