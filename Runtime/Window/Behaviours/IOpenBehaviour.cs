using DG.Tweening;

namespace Deaven.Ui.Window.Behaviours
{
    public interface IOpenBehaviour
    {
        public Tween Open(UiOpenCloseable target);
    }
}