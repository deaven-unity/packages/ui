using DG.Tweening;

namespace Deaven.Ui.Window.Behaviours
{
    public interface ICloseBehaviour
    {
        public Tween Close(UiOpenCloseable target);
    }
}