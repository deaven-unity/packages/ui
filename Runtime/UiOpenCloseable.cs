using UnityEngine;

namespace Deaven.Ui
{
    [RequireComponent(typeof(CanvasGroup), typeof(RectTransform))]
    [RequireComponent(typeof(Canvas))]
    public class UiOpenCloseable : MonoBehaviour
    {
        [SerializeField] protected Canvas canvas;
        protected CanvasGroup canvasGroup;
        protected RectTransform rectTransform;

        public CanvasGroup CanvasGroup => canvasGroup;

        public RectTransform RectTransform => rectTransform;

        public Canvas Canvas => canvas;
        
        private void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();
            rectTransform = GetComponent<RectTransform>();
            canvas = GetComponent<Canvas>();
        }
    }
}